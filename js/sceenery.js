/**
 * Created with JetBrains WebStorm.
 * User: christopherregan
 * Date: 6/24/12
 * Time: 6:50 PM
 * To change this template use File | Settings | File Templates.
 */
function sceenery($scope, $http){
   $scope.sceenes = []
    $scope.properties = [];
    $scope.propertyIds = [];
    $scope.currentItemSrc;
    $scope.currentProperty = 1;
    $scope.userId=26;
    $scope.USER_LOGGED_IN = new Boolean(false);
    $scope.catName = "gossip";
    $scope.stickers = [["gossip", "orange"], ["for sale", "red"], ["wardrobe", "brown"], ["promo", "green"], ["fan fiction", "yellow"], ["cast", "blue"], ["production stills", "orangered"], ["talk", "comment-blue"] ]
    $scope.gossipCount = 0;
    $scope.saleCount = 0;
    $scope.wardrobeCount = 0;
    $scope.castCount = 0;
    $scope.fictionCount = 0;
    $scope.prodCount = 0;
    $scope.promoCount = 0;

    $scope.init = function(){

        $('#mainSearch').typeahead({
            source: $scope.properties
        })

        $('#mainSearch').keyup(function(){
           $scope.inputVal = $('#mainSearch').val();
            $scope.searchProperty();
            //setTimeout(function(){$scope.clearPropArray()}, 500);
        });

        $('#signUpBtn').click(function(){
            $scope.addUser();
            $('#signUpForm').css("opacity", .5);
            $('#addBookmark').css("opacity", 1);
            $("#bookmarkComp").mousedown(function(){
                $("#startSceeningMessage").css("opacity", 1);
                $('#addBookmark').css("opacity",.5);
            })

            });
    }

    $scope.clearPropArray = function(){
        $scope.properties.length = 0;
        $scope.propertyIds.length = 0;

    }

    $scope.getSceenes = function(){

        for(var a=0; a<$scope.properties.length; a++){
           $("#titleField").text($scope.inputVal);


                if($scope.inputVal==$scope.properties[a])
                {
                    $scope.currentProperty = $scope.propertyIds[a];
                    //alert($scope.currentProperty);
                    $http({
                        method:'GET',
                        url:'sceeneryrest/getallcontent?propertyId=' + $scope.currentProperty}).success( function(data){
                            $scope.processSceenes(data);
                        })
                        .error( function(data){
                            alert("sorry, server error");
                            //$scope.processContent(madeUpData);
                        })
                    break;

                }
                else{
                    alert("no property associated with this title");
                    //do something with keywords;
                }
            }


    }

    $scope.searchProperty = function(){

        if($scope.inputVal==undefined)
        {
            alert("the value is undefined");
        }
        else{
            $http({
                method:'GET',
                url: 'sceeneryrest/searchproperty?propertyTitle=' + $scope.inputVal
            }).success(function(data){
                    $scope.processProperties(data);
                }).error(function(data){
                    alert("somethings fucked")
                });

        }

    }

    $scope.doLogin = function(){
        $scope.userEmail = $('#emailLogin').val();
        $scope.userPw = $('#passwordLogin').val();
        $scope.userData = {"email": $scope.userEmail, "password": $scope.userPw};
        $http({
            method:'POST',
            url: 'sceeneryuserrest/user/login',
            data: $scope.userData,
            headers: {'Content-Type': 'application/json'}
        }).success(function(data){
                $scope.USER_LOGGED_IN = true;
               $scope.doLoggedIn();
        }).error(function(data){
            alert("user not logged in")
            });
    }

    $scope.doLoggedIn = function(){
        $('#getBookMarkButton').css('display', 'none')
    }

    $scope.addUser = function(){
        //alert("Adding user")
        $scope.userEmail = $('#email').val();
        $scope.userPw = $('#password').val();
        $scope.userData = {"email": $scope.userEmail, "password": $scope.userPw}
        $http({
            method:'POST',
            url: 'sceeneryuserrest/user',
            data: $scope.userData,
            headers: {'Content-Type': 'application/json'}
        }).success(function(data){

        }).error(function(data){
                //alert("could not add user because you're a douche")
              //there was an error with setting up a new user.
        });
    };

    $scope.processProperties = function(data){

        $scope.properties.length = 0;
        $scope.propertyIds.length = 0;

        $scope.searchD = data;

        for(var t = 0; t<$scope.searchD.length; t++)
        {
            $scope.properties.push($scope.searchD[t].propertyTitle);
            $scope.propertyIds.push($scope.searchD[t].propertyId);

            $scope.currentProperty = $scope.propertyIds[0];
            $scope.inputVal = $scope.properties[0];
        }

        $scope.getSceenes();


        //alert($scope.searchD[0].propertyTitle);
    }

    $scope.getClass = function(item){
        //alert(item.contentSrc);
        //alert("Assign class");
        var theClass = "sticker sticker-share ";
        theCat = $scope.stickers[item.category.categoryId];

        theClass += theCat[1];
        $scope.catName = theCat[0];


        return theClass;
    }

    $scope.processSceenes = function(data){
        //alert("processing sceenes");
        $scope.jSONd = data;
        $scope.provisionalData = $scope.jSONd;
        $scope.sceenes = [];
        for(var n =0; n<$scope.provisionalData.length; n++)
        {
            if($scope.provisionalData[n].category.categoryId!==7)
            {
                $scope.sceenes.push($scope.provisionalData[n]);
            }
        }
        //$scope.sceenes = $scope.jSONd;
        $scope.totalCount = 0;
        $scope.gossipCount = 0;
        for(var q=0; q<$scope.sceenes.length; q++){
                    $scope.totalCount++;
          //alert($scope.sceenes[q].categoryId)
            if($scope.sceenes[q].category.categoryId==1){
                $scope.gossipCount++;
            }

        }

        $("#totalFilterBtn").text($scope.totalCount);
        //$("#imageFilterBtn").text($scope.gossipCount);
        //$scope.gossipCount;
    }

    $scope.assignCurrentItem = function(data){
        //$scope.currentItemSrc = data;
        $('#contentModalImage').attr('src', data);

    }

    $scope.getCatName = function(item){
        theCat = $scope.stickers[item.category.categoryId];

        theName += theCat[1];

        return theName;


    }

    $scope.init();
}