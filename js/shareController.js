/**
 * Created with JetBrains WebStorm.
 * User: christopherregan
 * Date: 8/26/12
 * Time: 11:53 AM
 * To change this template use File | Settings | File Templates.
 */
var winLocation;
function shareController($scope, $http){
    $scope.currentItemSrc;
    $scope.propertyTitle;
    $scope.currentItem;
    $scope.itemArray = [];

    $scope.shareProperties = [];
    $scope.sharePropertyIds = [];
    $scope.currentShareProperty = 1;
    $scope.shareInputVal = "";
    $scope.stickers = [["gossip", "orange"], ["for sale", "red"], ["wardrobe", "brown"], ["promo", "green"], ["fan fiction", "yellow"], ["cast", "blue"], ["production stills", "orangered"]]
    $scope.contentType = 1; ;
    $scope.following = ["Modern Family", "House", "The Color Purple", "The Usual Suspects"];
    $scope.desc = "";

    $scope.init = function(){

        $('#titleField').typeahead({
            source: $scope.shareProperties,
            updater:function (item) {

                $scope.onSelectTitle(item);
                return item;
            }
            //: function(obj) { $scope.onSelectTitle(obj) }

        })
        $('#titleField').keyup(function(){
            $scope.shareInputVal = $('#titleField').val();
            $scope.shareSearchProperty();
            //setTimeout(function(){$scope.clearPropArray()}, 500);
        });

        //$("ul.typeahead.dropdown-menu").find('li.active').data('value');
    }

    $scope.getClass = function(item){
        //alert("Assign class");
        var theClass = "sticker sticker-share ";
        theClass += item[1];


        return theClass;
    }


    $scope.assignTitle = function(title){
       $('#titleField').val(title);
        $scope.shareInputVal = $('#titleField').val();
        $scope.shareSearchProperty();
        $scope.onSelectTitle(title);
    }

    $scope.assignSticker = function(sticker){
           $scope.contentType = $scope.stickers.indexOf(sticker);
        //alert($scope.stickers.indexOf(sticker));
    }

    $scope.onSelectTitle = function(selectedItem){

        for (var t=0; t<$scope.shareProperties.length; t++){
            if($scope.shareProperties[t]==selectedItem)
            {
                $scope.currentShareProperty = $scope.sharePropertyIds[t];
            }

        }
        //alert($scope.currentShareProperty);
    }

    $scope.shareSearchProperty = function(){

        if($scope.shareInputVal==undefined)
        {
            alert("the value is undefined");
        }
        else{
            $http({
                method:'GET',
                url: 'sceeneryrest/searchproperty?propertyTitle=' + $scope.shareInputVal
            }).success(function(data){
                    $scope.processShareProperties(data);
                }).error(function(data){
                    alert("somethings fucked")
                });

        }

    }

    $scope.processShareProperties = function(data){

        $scope.shareProperties.length = 0;
        $scope.sharePropertyIds.length = 0;

        $scope.searchD = data;

        for(var t = 0; t<$scope.searchD.length; t++)
        {
            $scope.shareProperties.push($scope.searchD[t].propertyTitle);
            $scope.sharePropertyIds.push($scope.searchD[t].propertyId);
            //$scope.currentProperty = $scope.propertyIds[0];
            //$scope.inputVal = $scope.properties[0];
        }

        //$scope.getSceenes();


        //alert($scope.searchD[0].propertyTitle);
    }

    $scope.getContent = function(){
        winLocation = window.location;
        var query = $scope.getQueryVariable("location");
        $http({method:'GET', url:'sceeneryrest/parsehtml?url=' + query}).
        success( function(data){
            $scope.processContent(data);
        })
            .error( function(data){
                var madeUpData ={"htmlMediaList":[{"src":"http://graphics8.nytimes.com/adx/images/ADS/30/90/ad.309036/NYT_MJ_FALL_AD_LEFT-2-8.22.jpg"},{"src":"http://i1.nyt.com/images/2012/08/26/nyregion/26MOTH_CHARACTER/26MOTH_CHARACTER-moth.jpg"},{"src":"http://graphics8.nytimes.com/images/2010/09/16/opinion/Kristof_New/Kristof_New-thumbStandard-v2.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/26/us/child/child-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/25/arts/25SUMMER1/25SUMMER1-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/26/arts/26FAXON1_SPAN/26FAXON1_SPAN-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/26/nyregion/ALBANY1-caption-update/ALBANY1-caption-update-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/26/science/26armstrong-span/26armstrong-span-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/26/arts/26JAKE1/26JAKE1-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/26/travel/26-frugal-span/26-frugal-span-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/20/arts/jpwatch1/jpwatch1-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/29/dining/29KITCH_SPAN/29KITCH_SPAN-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/26/books/review/0826-bks-COVER/0826-bks-COVER-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/22/us/OBAMA/OBAMA-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/23/garden/23GUEST2_SPAN/23JPGUEST2-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2010/09/16/opinion/Friedman_New/Friedman_New-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com//images/2012/08/26/sunday-review/herd-promo-75.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/26/sunday-review/26longevity/26longevity-hpSmall.jpg"},{"src":"http://graphics8.nytimes.com/adx/images/ADS/29/65/ad.296504/12-0550_DigitalSub_163x90.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/26/magazine/26talk/26talk-thumbStandard-v2.jpg"},{"src":"http://graphics8.nytimes.com/adx/images/ADS/30/02/ad.300226/12-0446_DigitalSubGift_163x90.jpg"},{"src":"http://graphics8.nytimes.com/adx/images/ADS/30/90/ad.309034/NYT_MBMJ_FALL_AD_RIGHT_8.22.jpg"},{"src":"http://i1.nyt.com/images/misc/nytlogo379x64.gif"},{"src":"http://graphics8.nytimes.com/images/2012/08/24/t-magazine/24chic/24chic-thumbStandard.jpg"}],"text":"Subscribe: Digital / Home Delivery Log In Register Now Home Page Today's Paper Video Most Popular Edition: U.S. / Global Saturday, August 25, 2012 Last Update: 8:09 PM ET Subscribe to Home Delivery | Personalize Your Weather Follow Us | World U.S. Politics New York Business Dealbook Technology Sports Science Health Arts Style Opinion Autos Blogs Books Cartoons Classifieds Crosswords Dining & Wine Education Event Guide Fashion & Style Home & Garden Jobs Magazine Movies Music Obituaries Public Editor Real Estate Sunday Review T Magazine Television Theater Travel Weddings / Celebrations Multimedia Interactives Photography Podcasts Video Tools & more Alerts Beta 620 Corrections Mobile Movie Tickets Learning Network NYT Events NYT Store Theater Tickets Times Machine TimesLimited Times Skimmer Times Topics Times Wire Subscriptions Home Delivery Digital Subscriptions Gift Subscriptions Corporate Subscriptions Education Rate Crosswords Home Delivery Customer Care Replica Edition International Herald Tribune Company info About NYT Co. About IHT Advertise   For Big Givers, Cash and Clout Arrive as One at Convention By NICHOLAS CONFESSORE 2:46 PM ET Lobbyists, corporate executives, trade associations and donors exploit legal loopholes, making each party’s conventions a gathering of money and influence unrivaled in politics. The Caucus With Storm Coming, G.O.P. Postpones 1st Day of Convention By MICHAEL D. SHEAR 55 minutes ago The Republican National Convention will officially open on Monday but will immediately recess until Tuesday because of the expectation of bad weather. FiveThirtyEight: Romney's 'Likely Voter Gap' 7:32 PM ET News Analysis Apple-Samsung Case Shows Smartphone as Legal Magnet By STEVE LOHR The smartphone patent wars are unusually complex, and the courts are pushing companies toward a truce. Consumers may be the losers in the end. Document: Jury's Decision Bystanders’ Wounds in Shooting Caused by New York Police By MICHAEL WILSON 1 minute ago Nine people wounded in the shooting outside the Empire State Building on Friday were hit by police gunfire, Police Commissioner Raymond W. Kelly said. Long Before Carnage, an Office Grudge Festered Gunman Dies After Killing at Empire State Building NASA/Associated Press Neil Alden Armstrong, 1930-2012 Inspired Mankind With One Small Step By MARC SANTORA 6:14 PM ET Mr. Armstrong, who died Saturday at 82, became a global hero and made “one giant leap for mankind” as the first human to step foot on the moon. Colleagues and Stargazers Hail Armstrong on Twitter 7:24 PM ET Video Feature: Mission to the Moon | Slide Show: A Moon Odyssey Adding to Albany’s Scandals, Senator Faces Arrest By THOMAS KAPLAN 4:05 PM ET A day after Assemblyman Vito J. Lopez was censured, Shirley L. Huntley said she would be arrested in a corruption inquiry. Dozens of Bodies Found in Town Outside Damascus By DAMIEN CAVE and HWAIDA SAAD 14 minutes ago Daraya has been the focus of what activists described as a scorched-earth campaign to wipe out rebels. Revolts in Parts of Afghanistan Repel Taliban By ALISSA J. RUBIN and MATTHEW ROSENBERG Many Afghan villagers have come to loathe the Taliban for their dictatorial cruelty and are fighting back. Trial to Begin in Amish Beard-Cutting Attacks By ERIK ECKHOLM Samuel Mullet Sr., who leads a breakaway Amish group, has been charged along with 15 others of multiple counts including conspiracy, kidnapping and hate crimes. More News With Just Enough, Mets End Skid 37 minutes ago Storm Leaves Several Dead in Haiti 14 minutes ago French Leader Hails Greeks’ ‘Painful Efforts’ Video » More Video Multimed"}
                $scope.processContent(madeUpData);

        })
    }

    $scope.getQueryVariable = function(variable) {
        var query = winLocation.search.substring(1); //window.location.search.substring(1);
        var vars = query.split('&');
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            if (decodeURIComponent(pair[0]) == variable) {
                return decodeURIComponent(pair[1]);
            }
        }
        console.log('Query variable %s not found', variable);
    }
      $scope.processContent = function(data){
          $scope.jSONd = data; //{"htmlMediaList":[{"src":"http://graphics8.nytimes.com/adx/images/ADS/30/90/ad.309036/NYT_MJ_FALL_AD_LEFT-2-8.22.jpg"},{"src":"http://i1.nyt.com/images/2012/08/26/nyregion/26MOTH_CHARACTER/26MOTH_CHARACTER-moth.jpg"},{"src":"http://graphics8.nytimes.com/images/2010/09/16/opinion/Kristof_New/Kristof_New-thumbStandard-v2.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/26/us/child/child-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/25/arts/25SUMMER1/25SUMMER1-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/26/arts/26FAXON1_SPAN/26FAXON1_SPAN-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/26/nyregion/ALBANY1-caption-update/ALBANY1-caption-update-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/26/science/26armstrong-span/26armstrong-span-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/26/arts/26JAKE1/26JAKE1-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/26/travel/26-frugal-span/26-frugal-span-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/20/arts/jpwatch1/jpwatch1-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/29/dining/29KITCH_SPAN/29KITCH_SPAN-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/26/books/review/0826-bks-COVER/0826-bks-COVER-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/22/us/OBAMA/OBAMA-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/23/garden/23GUEST2_SPAN/23JPGUEST2-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com/images/2010/09/16/opinion/Friedman_New/Friedman_New-thumbStandard.jpg"},{"src":"http://graphics8.nytimes.com//images/2012/08/26/sunday-review/herd-promo-75.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/26/sunday-review/26longevity/26longevity-hpSmall.jpg"},{"src":"http://graphics8.nytimes.com/adx/images/ADS/29/65/ad.296504/12-0550_DigitalSub_163x90.jpg"},{"src":"http://graphics8.nytimes.com/images/2012/08/26/magazine/26talk/26talk-thumbStandard-v2.jpg"},{"src":"http://graphics8.nytimes.com/adx/images/ADS/30/02/ad.300226/12-0446_DigitalSubGift_163x90.jpg"},{"src":"http://graphics8.nytimes.com/adx/images/ADS/30/90/ad.309034/NYT_MBMJ_FALL_AD_RIGHT_8.22.jpg"},{"src":"http://i1.nyt.com/images/misc/nytlogo379x64.gif"},{"src":"http://graphics8.nytimes.com/images/2012/08/24/t-magazine/24chic/24chic-thumbStandard.jpg"}],"text":"Subscribe: Digital / Home Delivery Log In Register Now Home Page Today's Paper Video Most Popular Edition: U.S. / Global Saturday, August 25, 2012 Last Update: 8:09 PM ET Subscribe to Home Delivery | Personalize Your Weather Follow Us | World U.S. Politics New York Business Dealbook Technology Sports Science Health Arts Style Opinion Autos Blogs Books Cartoons Classifieds Crosswords Dining & Wine Education Event Guide Fashion & Style Home & Garden Jobs Magazine Movies Music Obituaries Public Editor Real Estate Sunday Review T Magazine Television Theater Travel Weddings / Celebrations Multimedia Interactives Photography Podcasts Video Tools & more Alerts Beta 620 Corrections Mobile Movie Tickets Learning Network NYT Events NYT Store Theater Tickets Times Machine TimesLimited Times Skimmer Times Topics Times Wire Subscriptions Home Delivery Digital Subscriptions Gift Subscriptions Corporate Subscriptions Education Rate Crosswords Home Delivery Customer Care Replica Edition International Herald Tribune Company info About NYT Co. About IHT Advertise   For Big Givers, Cash and Clout Arrive as One at Convention By NICHOLAS CONFESSORE 2:46 PM ET Lobbyists, corporate executives, trade associations and donors exploit legal loopholes, making each party’s conventions a gathering of money and influence unrivaled in politics. The Caucus With Storm Coming, G.O.P. Postpones 1st Day of Convention By MICHAEL D. SHEAR 55 minutes ago The Republican National Convention will officially open on Monday but will immediately recess until Tuesday because of the expectation of bad weather. FiveThirtyEight: Romney's 'Likely Voter Gap' 7:32 PM ET News Analysis Apple-Samsung Case Shows Smartphone as Legal Magnet By STEVE LOHR The smartphone patent wars are unusually complex, and the courts are pushing companies toward a truce. Consumers may be the losers in the end. Document: Jury's Decision Bystanders’ Wounds in Shooting Caused by New York Police By MICHAEL WILSON 1 minute ago Nine people wounded in the shooting outside the Empire State Building on Friday were hit by police gunfire, Police Commissioner Raymond W. Kelly said. Long Before Carnage, an Office Grudge Festered Gunman Dies After Killing at Empire State Building NASA/Associated Press Neil Alden Armstrong, 1930-2012 Inspired Mankind With One Small Step By MARC SANTORA 6:14 PM ET Mr. Armstrong, who died Saturday at 82, became a global hero and made “one giant leap for mankind” as the first human to step foot on the moon. Colleagues and Stargazers Hail Armstrong on Twitter 7:24 PM ET Video Feature: Mission to the Moon | Slide Show: A Moon Odyssey Adding to Albany’s Scandals, Senator Faces Arrest By THOMAS KAPLAN 4:05 PM ET A day after Assemblyman Vito J. Lopez was censured, Shirley L. Huntley said she would be arrested in a corruption inquiry. Dozens of Bodies Found in Town Outside Damascus By DAMIEN CAVE and HWAIDA SAAD 14 minutes ago Daraya has been the focus of what activists described as a scorched-earth campaign to wipe out rebels. Revolts in Parts of Afghanistan Repel Taliban By ALISSA J. RUBIN and MATTHEW ROSENBERG Many Afghan villagers have come to loathe the Taliban for their dictatorial cruelty and are fighting back. Trial to Begin in Amish Beard-Cutting Attacks By ERIK ECKHOLM Samuel Mullet Sr., who leads a breakaway Amish group, has been charged along with 15 others of multiple counts including conspiracy, kidnapping and hate crimes. More News With Just Enough, Mets End Skid 37 minutes ago Storm Leaves Several Dead in Haiti 14 minutes ago French Leader Hails Greeks’ ‘Painful Efforts’ Video » More Video Multimed"}
          $scope.items = $scope.jSONd.htmlMediaList;
          /*for (i = 0; i<jSONd.htmlMediaList.length; i++){
              var itemId = "shareItem" + i;
              document.getElementById('shareItems').innerHTML += "<a class='shareImageItem' id='{{itemId}}'><img ng-click='doItemClick()' src=" + "'" +  jSONd.htmlMediaList[i].src + "'" + " /></a>";
          }
          document.getElementById('shareItems').innerHTML += "<p>" +  jSONd.text + "</p>"*/;
      }

    $scope.assignCurrentItem = function(data){
       $scope.currentItemSrc = data.src;


    }

    $scope.onSubmit = function(){
        $scope.propertyTitle = document.getElementById('titleField').value;
        $scope.desc = document.getElementById('descField').value;
        $('#descField').val("");
        $scope.saveContent()
    }

    $scope.saveContent = function(title)
    {
        $scope.theData = {"contentSrc" : $scope.currentItemSrc, "property":{"propertyId":$scope.currentShareProperty}, "category": {"categoryId":$scope.contentType}, "contentDescription": $scope.desc};

       $http({
           method:'POST',
           url:'sceeneryrest/savecontent',
           data: $scope.theData,
           headers: { 'Content-Type': 'application/json'}
           }).
            success( function(data, status, headers, config){
                $scope.onContentSaved(data);
            })
            .error( function(data, status, headers, config){
                $scope.onContentSaved();
            })
    }

    $scope.doComment = function(data){
       $scope.currentItemSrc = data.contentDescription;
        $scope.currentItem = data.contentId;
        $scope.contentType = 7;
        $scope.desc = "";

       $scope.saveContent(data.propertyId);

    }

    $scope.linkContents = function(data){

        $http({
            method:'POST',
            url:'sceeneryrest/savecontentlink?contentId=' + $scope.currentItem + '&linkedContentId=' + data.contentId
        }).
            success( function(data){
                $scope.closeModal();
            })
            .error( function(data){
                $scope.closeModal();
            })

    }

    $scope.closeModal = function(){
        $('#myModal').modal('toggle');
        document.getElementById($scope.currentItemSrc).setAttribute("disabled", true);

    }

    $scope.onContentSaved = function(data){

        //this is verrrrrry hacky. lets try to avoid using empty strings and hardcoded shit in the future, ok, hackypants? ok.
        //if there's a description, make sure you save the comment as its own piece of content.
        if($scope.desc!="")
        {
            $scope.doComment(data);
        }
        //once that shit is done, crack a brewsky and link the new comment to the original piece of content
        else if(data.category.categoryId==7){
           $scope.linkContents(data);
        }
        else{
            $scope.closeModal();
        }

    }

    $scope.init();
    $scope.getContent();

}